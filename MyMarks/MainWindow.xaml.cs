﻿using System;
using System.Windows;

namespace MyMarks
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();          
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                viewModel = XMLTools.ReadFromXmlFile<ViewModel>("data.xml");
            }
            catch (Exception)
            {
                viewModel = new ViewModel();
            }

            this.DataContext = viewModel;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                XMLTools.WriteToXmlFile<ViewModel>("data.xml", viewModel);
            }
            catch (Exception)
            {                
            }
        }        
    }
}
