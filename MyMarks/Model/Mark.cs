﻿using System;
using System.ComponentModel;

namespace MyMarks.Model
{
    public class Mark : INotifyPropertyChanged
    {
        public Mark()
        {
            name = "";
            weight = 1;
            score = 0;
            IsMarked = false;
        }

        private string name;
        public String Name
        {
            get { return name; }
            set { name = value; NotifyPropertyChange("Name"); }
        }

        private float weight;
        public float Weight
        {
            get { return weight; }
            set { weight = value; NotifyPropertyChange("Weight"); }
        }

        private float score;
        public float Score
        {
            get { return score; }
            set { score = value; NotifyPropertyChange("Score"); }
        }

        private bool isMarked;
        public bool IsMarked
        {
            get { return isMarked; }
            set { isMarked = value; NotifyPropertyChange("IsMarked"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
