﻿using System.ComponentModel;

namespace MyMarks.Model
{
    public class Course : INotifyPropertyChanged
    {
        public Course()
        {
            Name = "";
            Credits = 0;
            Marks = new ItemwideObservableCollection<Mark>();
            Marks.CollectionChanged += MarkesChanged;
        }
        public Course(string name, float credits)
        {
            Name = name;
            Credits = credits;
            Marks = new ItemwideObservableCollection<Mark>();
            Marks.CollectionChanged += MarkesChanged;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void MarkesChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NotifyPropertyChange("TotalMarks");
        }

        private string name;
        public string Name { get { return name; } set { name = value; NotifyPropertyChange("Name"); } }
        public float Credits { get; set; }
        public ItemwideObservableCollection<Mark> Marks { get; set; }

        public float TotalMark {
            get
            {
                if (Marks.Count < 1)
                {
                    return 0;
                }

                float totalmark = 0;
                float totalweight = 0;
                foreach (Model.Mark mark in Marks)
                {
                    if (mark.IsMarked)
                    {
                        totalmark += mark.Score * mark.Weight;
                        totalweight += mark.Weight;
                    }
                }

                if (totalweight <= 0)
                {
                    return 0;
                }
               
                return totalmark/totalweight;
            }
        }
    }
}
