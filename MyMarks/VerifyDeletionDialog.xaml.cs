﻿using System.Windows;

namespace MyMarks
{
    /// <summary>
    /// Interaction logic for VerifyDeletionDialog.xaml
    /// </summary>
    public partial class VerifyDeletionDialog : Window
    {
        public VerifyDeletionDialog()
        {
            InitializeComponent();
        }

        private void BtnYes_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void BtnNo_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        public static bool CreateAndShow()
        {
            VerifyDeletionDialog dialog = new VerifyDeletionDialog();
            dialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            bool? result = dialog.ShowDialog();
            if (result.HasValue)
            {
                return result.Value;
            }
            else
            {
                return false;
            }
        }
    }
}
