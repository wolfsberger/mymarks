﻿using System.ComponentModel;
using System.Windows.Input;

namespace MyMarks
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            courses = new ItemwideObservableCollection<Model.Course>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChange(string eventName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(eventName));
        }

        private ItemwideObservableCollection<Model.Course> courses;
        public ItemwideObservableCollection<Model.Course> Courses {
            get { return courses; }
            private set { courses = value; NotifyPropertyChange("Courses"); }
        }

        private int selectedIndex;
        public int SelectedIndex {
            get { return selectedIndex; }
            set { selectedIndex = value; NotifyPropertyChange("SelectedIndex"); NotifyPropertyChange("SelectedCourse"); }
        }

        private void AddCourse(object param)
        {
            Courses.Add(new Model.Course("New Course", 15));
            SelectedIndex = Courses.Count - 1;
        }
        private void RemoveCourse(object param)
        {
            if (VerifyDeletionDialog.CreateAndShow())
            {
                Courses.Remove(SelectedCourse);
                SelectedIndex = -1;
            }
        }

        private ICommand addCourseCommand;
        public ICommand AddCourseCommand
        {
            get
            {
                if (addCourseCommand == null)
                {
                    addCourseCommand = new RelayCommand(AddCourse);
                }
                return addCourseCommand;
            }
        }

        private ICommand removeCourseCommand;
        public ICommand RemoveCourseCommand
        {
            get
            {
                if (removeCourseCommand == null)
                {
                    removeCourseCommand = new RelayCommand(RemoveCourse);
                }
                return removeCourseCommand;
            }
        }


        public Model.Course SelectedCourse {
            get {
                if (selectedIndex < 0 || selectedIndex >= courses.Count)
                {
                    return null;
                }                
                return courses[selectedIndex];
            } }
       
    }
}
