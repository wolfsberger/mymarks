﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace MyMarks
{
    class MarkToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SolidColorBrush brush = new SolidColorBrush();
            float floatValue = (float)value;
            Color color = Colors.Red;
            if (floatValue > 70)
            { color = Colors.Green; }
            else if (floatValue > 60)
            { color = Colors.YellowGreen; }
            else if (floatValue > 50)
            { color = Colors.Yellow; }
            else
            { color = Colors.Red; }

            color.A = 100;
            brush.Color = color;
            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0.0f;
        }
    }
}
